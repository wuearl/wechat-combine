<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\Pay\Kernel;

use EasyWeChat\Pay\Application;
use EasyWeChat\OnlineRetailer\Kernel\BaseClient as PaymentClient;

/**
 * Class BaseClient
 * @package EasyWeChat\Combine\Kernel
 */
class BaseClient extends PaymentClient
{
    /**
     * BaseClient constructor.
     *
     * @param \EasyWeChat\Pay\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        $this->setHttpClient($this->app['http_client']);
    }
}
