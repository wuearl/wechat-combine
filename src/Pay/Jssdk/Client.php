<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\Pay\Jssdk;

use EasyWeChat\BasicService\Jssdk\Client as JssdkClient;
use WechatPay\GuzzleMiddleware\Auth\PrivateKeySigner;
use WechatPay\GuzzleMiddleware\Util\PemUtil;

/**
 * Class Client.
 *
 * @author overtrue <i@overtrue.me>
 */
class Client extends JssdkClient
{
    /**
     * [WeixinJSBridge] Generate js config for payment.
     *
     * <pre>
     * WeixinJSBridge.invoke(
     *  'getBrandWCPayRequest',
     *  ...
     * );
     * </pre>
     *
     * @param string $prepayId
     * @param bool $json
     *
     * @return string|array
     */
    public function bridgeConfig(string $prepayId, bool $json = true)
    {
        $params = [
            'appId' => $this->app['config']->sub_appid ?: $this->app['config']->app_id,
            'timeStamp' => strval(time()),
            'nonceStr' => uniqid(),
            'package' => "prepay_id=$prepayId"
        ];

        $str = $params['appId'] . "\n" .
            $params['timeStamp'] . "\n" .
            $params['nonceStr'] . "\n" .
            $params['package'] . "\n";
        $params['signType'] = 'RSA';
        $params['paySign'] = $this->sign($str);
        return $json ? json_encode($params) : $params;
    }

    /**
     * [JSSDK] Generate js config for payment.
     *
     * <pre>
     * wx.chooseWXPay({...});
     * </pre>
     *
     * @param string $prepayId
     *
     * @return array
     */
    public function sdkConfig(string $prepayId): array
    {
        $config = $this->bridgeConfig($prepayId, false);

        $config['timestamp'] = $config['timeStamp'];
        unset($config['timeStamp']);

        return $config;
    }

    /**
     * Generate app payment parameters.
     *
     * @param string $prepayId
     *
     * @return array
     */
    public function appConfig(string $prepayId): array
    {
        $params = [
            'appid' => $this->app['config']->app_id,
            'partnerid' => $this->app['config']->mch_id,
            'prepayid' => $prepayId,
            'noncestr' => uniqid(),
            'timestamp' => time(),
            'package' => 'Sign=WXPay',
        ];

        $str = $params['appid'] . "\n" .
            $params['timestamp'] . "\n" .
            $params['noncestr'] . "\n" .
            $prepayId . "\n";
        $params['sign'] = $this->sign($str);
        return $params;
    }

    /**
     *
     * @param string $str
     * @return string
     */
    protected function sign(string $str)
    {
        $privateKey = PemUtil::loadPrivateKey($this->app['config']->key_path);
        $certificate = PemUtil::loadCertificate($this->app['config']->wx_cert_path);
        $serialNo = PemUtil::parseCertificateSerialNo($certificate);
        $encryptor = new PrivateKeySigner($serialNo, $privateKey);
        return $encryptor->sign($str)->getSign();
    }
}
