<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\Pay\Partner;

use EasyWeChat\Pay\Kernel\BaseClient;

/**
 * Class Client
 * @package EasyWeChat\OnlineRetailer\Combine
 */
class Client extends BaseClient
{
    /**
     * app.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function app($params)
    {
        return $this->httpPostJson('v3/pay/partner/transactions/app', $this->buildParams($params));
    }

    /**
     * 公众号|小程序.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function jsapi($params)
    {
        return $this->httpPostJson('v3/pay/partner/transactions/jsapi', $this->buildParams($params));
    }

    /**
     * h5.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function wap($params)
    {
        return $this->httpPostJson('v3/pay/partner/transactions/h5', $this->buildParams($params));
    }

    /**
     * 扫码支付.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function scan($params)
    {
        return $this->httpPostJson('v3/pay/partner/transactions/native', $this->buildParams($params));
    }

    /**
     * @param $params
     * @return array
     */
    protected function buildParams($params): array
    {
        $params['sp_appid'] = $this->app->config->get('app_id');
        $params['sp_mchid'] = $this->app->config->get('mch_id');
        if ($this->app->config->get('sub_appid')) {
            $params['sub_appid'] = $this->app->config->get('sub_appid');
        }
        $params['sub_mchid'] = $this->app->config->get('sub_mch_id');
        $params['notify_url'] = $params['notify_url'] ?? $this->app['config']['notify_url'];
        return $params;
    }

    /**
     * 查询订单.
     *
     * @param $out_trade_no
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $out_trade_no)
    {
        $params['sp_mchid'] = $this->app->config->get('mch_id');
        $params['sub_mchid'] = $this->app->config->get('sub_mch_id');
        return $this->httpGet('v3/pay/partner/transactions/out-trade-no/' . $out_trade_no);
    }

    /**
     * 关闭订单.
     *
     * @param string $out_trade_no
     * @param array $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function close(string $out_trade_no, array $params)
    {
        $params['sp_mchid'] = $this->app->config->get('mch_id');
        $params['sub_mchid'] = $this->app->config->get('sub_mch_id');
        return $this->httpPostJson('v3/pay/partner/transactions/out-trade-no/' . $out_trade_no . '/close', $params);
    }
}
