<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/7/14
 * Time: 14:12
 */

namespace EasyWeChat;

/**
 * Class Factory.
 *
 * @method static \EasyWeChat\Payment\Application              payment(array $config)
 * @method static \EasyWeChat\MiniProgram\Application          miniProgram(array $config)
 * @method static \EasyWeChat\OpenPlatform\Application         openPlatform(array $config)
 * @method static \EasyWeChat\OfficialAccount\Application      officialAccount(array $config)
 * @method static \EasyWeChat\BasicService\Application         basicService(array $config)
 * @method static \EasyWeChat\Work\Application                 work(array $config)
 * @method static \EasyWeChat\OpenWork\Application             openWork(array $config)
 * @method static \EasyWeChat\MicroMerchant\Application        microMerchant(array $config)
 * @method static \EasyWeChat\OnlineRetailer\Application       onlineRetailer(array $config)
 * @method static \EasyWeChat\ServiceProvider\Application      serviceProvider(array $config)
 * @method static \EasyWeChat\Pay\Application                  pay(array $config)
 */
class WeChat
{
    /**
     * @param string $name
     * @param array  $config
     *
     * @return \EasyWeChat\Kernel\ServiceContainer
     */
    public static function make($name, array $config)
    {
        $namespace = Kernel\Support\Str::studly($name);
        $application = "\\EasyWeChat\\{$namespace}\\Application";

        return new $application($config);
    }

    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}
