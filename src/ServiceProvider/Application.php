<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/7/14
 * Time: 13:54
 */

namespace EasyWeChat\ServiceProvider;

use EasyWeChat\Kernel\ServiceContainer;

/**
 * Class Application
 * @package EasyWeChat\OnlineRetailer
 * @property \EasyWeChat\ServiceProvider\SpecialSupplier\Client $special_supplier
 */
class Application extends ServiceContainer
{
    protected $providers = [
        SpecialSupplier\ServiceProvider::class
    ];
}