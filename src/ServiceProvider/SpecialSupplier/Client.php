<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\ServiceProvider\SpecialSupplier;

use EasyWeChat\ServiceProvider\Kernel\BaseClient;
use WechatPay\GuzzleMiddleware\Util\PemUtil;

/**
 * Class Client
 * @package EasyWeChat\OnlineRetailer\Combine
 */
class Client extends BaseClient
{
    /**
     * 提交申请
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function applyment($params)
    {
        $certificate = PemUtil::loadCertificate($this->app->config->get('wx_cert_path'));
        $serialNo = PemUtil::parseCertificateSerialNo($certificate);
        return $this->httpPostJson('https://api.mch.weixin.qq.com/v3/applyment4sub/applyment/', $params, [], [
            'Wechatpay-Serial' => $serialNo
        ]);
    }

    /**
     * 查询进度
     * @param $business_code
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query($business_code)
    {
        return $this->httpGet('https://api.mch.weixin.qq.com/v3/applyment4sub/applyment/business_code/' . $business_code);
    }
}
