<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\OnlineRetailer\Ecommerce;

use EasyWeChat\OnlineRetailer\Kernel\BaseClient;
use WechatPay\GuzzleMiddleware\Util\PemUtil;

/**
 * Class Client
 * @package EasyWeChat\OnlineRetailer\Ecommerce
 */
class Client extends BaseClient
{
    /**
     * 提交申请
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function applyment($params)
    {
        $certificate = PemUtil::loadCertificate($this->app->config->get('wx_cert_path'));
        $serialNo = PemUtil::parseCertificateSerialNo($certificate);
        return $this->httpPostJson('https://api.mch.weixin.qq.com/v3/ecommerce/applyments/', $params, [], [
            'Wechatpay-Serial' => $serialNo
        ]);
    }

    /**
     * 查询进度
     * @param $applyment_id
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query($applyment_id)
    {
        return $this->httpGet('https://api.mch.weixin.qq.com/v3/ecommerce/applyments/' . $applyment_id);
    }

    /**
     * 修改结算账户
     * @param $sub_mchid
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function modifySettlement($sub_mchid, $params)
    {
        $certificate = PemUtil::loadCertificate($this->app->config->get('wx_cert_path'));
        $serialNo = PemUtil::parseCertificateSerialNo($certificate);
        return $this->httpPostJson('https://api.mch.weixin.qq.com/v3/apply4sub/sub_merchants/' . $sub_mchid . '/modify-settlement', $params, [], [
            'Wechatpay-Serial' => $serialNo
        ]);
    }

    /**
     * 查询结算账户
     * @param $sub_mchid
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function querySettlement($sub_mchid)
    {
        return $this->httpGet('https://api.mch.weixin.qq.com/v3/apply4sub/sub_merchants/' . $sub_mchid . '/settlement');
    }

    /**
     * 实时余额.
     *
     * @param $sub_mchid
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function balance($sub_mchid)
    {
        return $this->httpGet('https://api.mch.weixin.qq.com/v3/ecommerce/fund/balance/' . $sub_mchid);
    }

    /**
     * 日终余额.
     *
     * @param $sub_mchid
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function endDayBalance($sub_mchid)
    {
        return $this->httpGet('https://api.mch.weixin.qq.com/v3/ecommerce/fund/enddaybalance/' . $sub_mchid);
    }

    /**
     * 余额提现.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function withdraw(array $params)
    {
        return $this->httpPostJson('https://api.mch.weixin.qq.com/v3/ecommerce/fund/withdraw', $params);
    }

    /**
     * 查询提现.
     *
     * @param $withdraw_id
     * @param $sub_mchid
     * @param string $out_request_no
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryWithdraw($withdraw_id, $sub_mchid, $out_request_no = '')
    {
        $url = 'https://api.mch.weixin.qq.com/v3/';
        if ($out_request_no) {
            $url .= 'ecommerce/fund/out_request_no/' . $out_request_no;
        } else {
            $url .= 'ecommerce/fund/withdraw/' . $withdraw_id;
        }
        return $this->httpGet($url, [
            'sub_mchid' => $sub_mchid
        ]);
    }

    /**
     * 请求分账.
     *
     * @param string $sub_mchid
     * @param string $transaction_id
     * @param string $out_order_no
     * @param array $receivers
     * @param bool $finish
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function profitSharing(string $sub_mchid, string $transaction_id, string $out_order_no, array $receivers, bool $finish = false)
    {
        $url = 'https://api.mch.weixin.qq.com/v3/ecommerce/profitsharing/orders';

        return $this->httpPostJson($url, [
            'appid' => $this->app->config->get('app_id'),
            'sub_mchid' => $sub_mchid,
            'transaction_id' => $transaction_id,
            'out_order_no' => $out_order_no,
            'receivers' => $receivers,
            'finish' => $finish,
        ]);
    }

    /**
     * 查询分账.
     *
     * @param string $sub_mchid
     * @param string $transaction_id
     * @param string $out_order_no
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryProfitSharing(string $sub_mchid, string $transaction_id, string $out_order_no)
    {
        $url = 'https://api.mch.weixin.qq.com/v3/ecommerce/profitsharing/orders';
        return $this->httpGet($url, [
            'sub_mchid' => $sub_mchid,
            'transaction_id' => $transaction_id,
            'out_order_no' => $out_order_no
        ]);
    }

    /**
     * 退回分账.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function returnProfitSharing($params)
    {
        $url = 'https://api.mch.weixin.qq.com/v3/ecommerce/profitsharing/returnorders';
        return $this->httpPostJson($url, $params);
    }

    /**
     * 查询退回分账.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryReturnProfitSharing($params)
    {
        $url = 'https://api.mch.weixin.qq.com/v3/ecommerce/profitsharing/returnorders';
        return $this->httpGet($url, $params);
    }

    /**
     * 完成分账.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function finishProfitSharing($params)
    {
        $url = 'https://api.mch.weixin.qq.com/v3/ecommerce/profitsharing/returnorders';
        return $this->httpPostJson($url, $params);
    }

    /**
     * 添加分账方.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addReceivers($params)
    {
        $url = 'https://api.mch.weixin.qq.com/v3/ecommerce/profitsharing/receivers/add';
        $headers = [];
        if (isset($params['encrypted_name'])) {
            $certificate = PemUtil::loadCertificate($this->app->config->get('wx_cert_path'));
            $serialNo = PemUtil::parseCertificateSerialNo($certificate);
            $headers['Wechatpay-Serial'] = $serialNo;
        }
        return $this->httpPostJson($url, $params, [], $headers);
    }

    /**
     * 删除分账方.
     *
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delReceivers($params)
    {
        $url = 'https://api.mch.weixin.qq.com/v3/ecommerce/profitsharing/receivers/delete';
        return $this->httpPostJson($url, $params);
    }
}
