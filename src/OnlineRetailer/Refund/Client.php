<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\OnlineRetailer\Refund;

use EasyWeChat\OnlineRetailer\Kernel\BaseClient;
use WechatPay\GuzzleMiddleware\Util\PemUtil;

/**
 * Class Client
 * @package EasyWeChat\OnlineRetailer\Ecommerce
 */
class Client extends BaseClient
{
    /**
     * @param $params
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function apply($params)
    {
        $params['sp_appid'] = $this->app->config->get('app_id');
        $params['notify_url'] = $params['notify_url'] ?? $this->app['config']['notify_url'];
        return $this->httpPostJson('https://api.mch.weixin.qq.com/v3/ecommerce/refunds/apply', $params);
    }

    /**
     * @param string $out_trade_no
     * @param $sub_mch_id
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $out_trade_no, $sub_mch_id)
    {
        return $this->httpGet('https://api.mch.weixin.qq.com/v3/ecommerce/refunds/out-refund-no/' . $out_trade_no, [
            'sub_mchid' => $sub_mch_id
        ]);
    }
}
