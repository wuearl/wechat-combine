<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\OnlineRetailer\Kernel;

use EasyWeChat\Kernel\Exceptions\InvalidArgumentException;
use EasyWeChat\Kernel\Support;
use EasyWeChat\OnlineRetailer\Application;
use EasyWeChat\Payment\Kernel\BaseClient as PaymentBaseClient;
use WechatPay\GuzzleMiddleware\Util\MediaUtil;
use WechatPay\GuzzleMiddleware\Util\PemUtil;
use WechatPay\GuzzleMiddleware\WechatPayMiddleware;

/**
 * Class BaseClient
 * @package EasyWeChat\Combine\Kernel
 */
class BaseClient extends PaymentBaseClient
{
    /**
     * BaseClient constructor.
     *
     * @param \EasyWeChat\OnlineRetailer\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        $this->setHttpClient($this->app['http_client']);
    }

    /**
     * Extra request params.
     *
     * @return array
     */
    protected function prepends()
    {
        return [];
    }

    /**
     * httpUpload.
     *
     * @param string $url
     * @param string $path
     * @param bool $returnResponse
     * @return array|Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpUpload(string $url, string $path, $returnResponse = false)
    {
        $media = new MediaUtil($path);
        $options = [
            'body' => $media->getStream(),
            'headers' => [
                'Accept' => 'application/json',
                'content-type' => $media->getContentType(),
            ]
        ];
        return $this->request($url, [], 'POST', $options);
    }

    /**
     * httpGet.
     *
     * @param string $url
     * @param array $query
     * @return array|Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpGet(string $url, array $query = [])
    {
        return $this->request($url, [], 'GET', ['query' => $query, 'headers' => ['Accept' => 'application/json']]);
    }

    /**
     * httpPostJson.
     *
     * @param string $url
     * @param array $json
     * @param array $query
     * @param array $headers
     * @return array|Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpPostJson(string $url, array $json = [], array $query = [], array $headers = [])
    {
        $headers = array_merge($headers, ['Accept' => 'application/json']);
        $options = [
            'query' => $query, 'json' => $json, 'headers' => $headers
        ];
        return $this->request($url, [], 'POST', $options);
    }

    /**
     * request.
     *
     * @param string $endpoint
     * @param array $params
     * @param string $method
     * @param array $options
     * @param bool $returnResponse
     *
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     *
     * @throws InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function request(string $endpoint, array $params = [], $method = 'POST', array $options = [], $returnResponse = false)
    {
        $this->pushMiddleware($this->logMiddleware(), 'log');
        $this->pushMiddleware($this->weChatPayMiddleware(), 'wechatpay');
        $response = $this->performRequest($endpoint, $method, $options);
        return $returnResponse ? $response : $this->castResponseToType($response, $this->app->config->get('response_type'));
    }

    /**
     *  WeChatPay the request.
     * @return WechatPayMiddleware
     */
    protected function weChatPayMiddleware()
    {
        $privateKey = PemUtil::loadPrivateKey($this->app->config->get('key_path'));
        $certificate = PemUtil::loadCertificate($this->app->config->get('wx_cert_path'));

        $certStr = PemUtil::loadCertificate($this->app->config->get('cert_path'));
        $serialNo = PemUtil::parseCertificateSerialNo($certStr);
        return WechatPayMiddleware::builder()
            ->withMerchant($this->app->config->get('mch_id'), $serialNo, $privateKey)
            ->withWechatPay([$certificate])
            ->build();
    }
}
