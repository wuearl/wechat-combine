<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\OnlineRetailer\Notify;

use Closure;
use EasyWeChat\Kernel\Exceptions\Exception;
use EasyWeChat\Kernel\Support;
use EasyWeChat\Payment\Kernel\Exceptions\InvalidSignException;
use Symfony\Component\HttpFoundation\Response;
use WechatPay\GuzzleMiddleware\Auth\CertificateVerifier;
use WechatPay\GuzzleMiddleware\Auth\WechatPay2Validator;
use WechatPay\GuzzleMiddleware\Util\AesUtil;
use WechatPay\GuzzleMiddleware\Util\PemUtil;

abstract class Handler
{
    const SUCCESS = 'SUCCESS';
    const FAIL = 'FAIL';

    /**
     * @var \EasyWeChat\OnlineRetailer\Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $message;

    /**
     * @var string|null
     */
    protected $fail;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Check sign.
     * If failed, throws an exception.
     *
     * @var bool
     */
    protected $check = true;

    /**
     * Respond with sign.
     *
     * @var bool
     */
    protected $sign = false;

    /**
     * @param \EasyWeChat\OnlineRetailer\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Handle incoming notify.
     *
     * @param \Closure $closure
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function handle(Closure $closure);

    /**
     * @param string $message
     */
    public function fail(string $message)
    {
        $this->fail = $message;
    }

    /**
     * @param array $attributes
     * @param bool $sign
     *
     * @return $this
     */
    public function respondWith(array $attributes, bool $sign = false)
    {
        $this->attributes = $attributes;
        $this->sign = $sign;

        return $this;
    }

    /**
     * Build xml and return the response to WeChat.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     */
    public function toResponse(): Response
    {
        $base = [
            'code' => is_null($this->fail) ? static::SUCCESS : static::FAIL,
            'message' => $this->fail,
        ];

        $attributes = array_merge($base, $this->attributes);

        if ($this->sign) {
            $attributes['sign'] = Support\generate_sign($attributes, $this->app->getKey());
        }

        return new Response(json_encode($base));
    }

    /**
     *  Return the notify message from request.
     *
     * @return array
     * @throws Exception
     * @throws InvalidSignException
     */
    public function getMessage(): array
    {
        if (!empty($this->message)) {
            return $this->message;
        }

        if ($this->check) {
//            $this->validate($this->app['request']);
        }

        try {
            $message = @json_decode($this->app['request']->getContent(), 1);
            if ($message['resource']) {
                $data = (new AesUtil($this->app->config->get('key_v3') ?? $this->app->config->get('key')))->decryptToString($message['resource']['associated_data'], $message['resource']['nonce'], $message['resource']['ciphertext']);
                $message = @json_decode($data, true);
                if (!is_array($message)) {
                    throw new Exception('Invalid request', 400);
                }
            }
        } catch (\Throwable $e) {
            throw new Exception('Invalid request XML: ' . $e->getMessage(), 400);
        }

        if (!is_array($message) || empty($message)) {
            throw new Exception('Invalid request XML.', 400);
        }

        return $this->message = $message;
    }

    /**
     * Decrypt message.
     *
     * @param string $key
     *
     * @return string|null
     *
     * @throws \EasyWeChat\Kernel\Exceptions\Exception
     */
    public function decryptMessage(string $key)
    {
        $message = $this->getMessage();
        if (empty($message[$key])) {
            return null;
        }

        return Support\AES::decrypt(
            base64_decode($message[$key], true),
            md5($this->app['config']->key),
            '',
            OPENSSL_RAW_DATA,
            'AES-256-ECB'
        );
    }

    /**
     * Validate the request params.
     *
     * @param $response
     * @throws InvalidSignException
     */
    protected function validate($response)
    {
        $certificate[] = PemUtil::loadCertificate($this->app->config->get('wx_cert_path'));
        $validator = new WechatPay2Validator(new CertificateVerifier($certificate));
        if (!$validator->validate($response)) {
            throw new InvalidSignException();
        }
    }

    /**
     * @param mixed $result
     */
    protected function strict($result)
    {
        if (true !== $result && is_null($this->fail)) {
            $this->fail(strval($result));
        }
    }
}
