<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyWeChat\OnlineRetailer\Media;

use EasyWeChat\OnlineRetailer\Kernel\BaseClient;

/**
 * Class Client
 * @package EasyWeChat\OnlineRetailer\Ecommerce
 */
class Client extends BaseClient
{
    /**
     * @param $path
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function upload($path)
    {
        return $this->httpUpload('https://api.mch.weixin.qq.com/v3/merchant/media/upload', $path);
    }

    /**
     * @param $path
     * @return array|\EasyWeChat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function videoUpload($path)
    {
        return $this->httpUpload('https://api.mch.weixin.qq.com/v3/merchant/media/video_upload', $path);
    }
}
